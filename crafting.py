"""
Minecraft inventory + crafting system.
I swear this is for educational purposes.

Author: Simon Vandevelde <s.vandevelde@kuleuven.be>
"""

class Inventory():
    def __init__(self):
        self.items = {'wood': 0, 'planks': 0, 'sticks': 0,
                      'cobblestone': 0, 'wooden_pickaxe': 0,
                      'stone_pickaxe': 0, 'iron': 0,
                      'iron_pickaxe': 0}

    def number_of(self, item):
        return self.items[item]

    def add_item(self, item, number):
        self.items[item] += number

class Chest(Inventory):
    def __init__(self):
        self.__super__()

class PlayerInventory(Inventory):
    def __init__(self):
        super().__init__()
        self.has_crafting_table = False

    def create_planks(self, number):
        # Each wood creates 4 planks.
        if self.items['wood'] >= number / 4:
            self.items['wood'] -= number / 4
            self.items['planks'] += number
        else:
            raise Exception('Not enough wood')

    def create_sticks(self, number):
        # 2 planks make 4 sticks
        if self.items['planks'] > number/2:
            self.items['planks'] -= number/2
            self.times['sticks'] += number

    def create_crafting_table(self):
        # Requires 4 wood.
        if self.items['planks'] >= 4:
            self.items['planks'] -= 4
            self.has_crafting_table = True
        else:
            raise Exception('Not enough planks')

    def create_wooden_pickaxe(self):
        # Requires 3 wood, 2 sticks
        self.items['wood'] -= 3
        self.items['sticks'] -= 2
        self.items['wooden_pickaxe'] += 1

    def create_stone_pickaxe(self):
        if not self.has_crafting_table:
            raise Exception('Need crafting table')
        # Requires 3 stone, 2 sticks
        self.items['stone'] -= 2
        self.items['sticks'] -= 2
        self.items['stone_pickaxe'] += 1

    def create_iron_pickaxe(self):
        # Requires 3 iron, 2 sticks
        self.items['stone'] -= 3
        self.items['sticks'] -= 2
        self.items['stone_pickaxe'] += 1


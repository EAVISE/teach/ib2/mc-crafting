import unittest
from crafting import Inventory, Chest, PlayerInventory


class TestCrafting(unittest.TestCase):

    def test_add_items(self):
        # Testen of items invoegen werkt.
        player = PlayerInventory()
        player.add_item('wood', 4)
        self.assertEqual(player.number_of('wood'), 4)
        player.add_item('planks', 69)
        self.assertEqual(player.number_of('planks'), 69)
        player.add_item('sticks', 420)
        self.assertEqual(player.number_of('sticks'), 420)

    def test_planks(self):
        player = PlayerInventory()
        player.add_item('wood', 4)
        player.create_planks(4)
        # Heb ik nu 4 planks?
        self.assertEqual(player.number_of('planks'), 4)

        # Is het hout verminderd met 1?
        self.assertEqual(player.number_of('wood'), 3)

    def test_create_table_success(self):
        player = PlayerInventory()
        player.add_item('planks', 4)
        # Kan ik een crafting table maken als ik 4 planks heb?
        player.create_crafting_table()
        self.assertTrue(player.has_crafting_table)

    def test_create_table_failure(self):
        player = PlayerInventory()
        player.add_item('planks', 2)
        # Wat gebeurt er als ik niet 4 planks heb?
        with self.assertRaises(Exception):
            player.create_crafting_table()
        self.assertFalse(player.has_crafting_table)

if __name__ == '__main__':
    unittest.main()



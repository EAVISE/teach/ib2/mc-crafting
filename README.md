# MC-Crafting

Project used in the IB2 lab to teach unit testing.

Goal is to set up unit tests for a MC crafting system.
I.e., when creating planks, we can test that (a) there is wood available, (b) the number of wood reduces and (c) the number of planks increases.


